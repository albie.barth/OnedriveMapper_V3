﻿#https://deletethis.net/dave/ David_Risney is the builder
$Jobs = @()
$SleepTimer = 50
$RunspacePool = [runspacefactory]::CreateRunspacePool(1, 2, [system.management.automation.runspaces.initialsessionstate]::CreateDefault(), $Host)
$RunspacePool.ApartmentState = "STA"
$RunspacePool.Open()
$communicatieObject = [hashtable]::Synchronized(@{})
$communicatieObject.main = @{}
$communicatieObject.main.description = "datacommunicatie tussen threads in de runspace"
$communicatieObject.functionCalls = @{}
$communicatieObject.jobList = @("form2.ps1")
$communicatieObject.jobParameters = @{}
$communicatieObject.jobParameters.0 = @{"communicatieObject" = $communicatieObject}

$script:jobIndex = 0
$script:finishedJobs = 0
$script:runningJobs = 0
$script:waitingJobs = @($communicatieObject.jobList).Count
$script:totalJobs = @($communicatieObject.jobList).Count

function startThread(){
    Param(
        [String]$fileName,
        [Hashtable]$communicatieObject,
        [Hashtable]$parameters
    )
    $secondaryScriptPath = Join-Path -Path $(if ($psISE) {Split-Path -Path $psISE.CurrentFile.FullPath} else {$(if ($global:PSScriptRoot.Length -gt 0) {$global:PSScriptRoot} else {$global:pwd.Path})}) -ChildPath $fileName
    $OFS = "`r`n"
    $Code = [ScriptBlock]::Create($(Get-Content $secondaryScriptPath))
    if($Code -eq $Null){
        Throw "File does not exist or is empty at $secondaryScriptPath"
    }
    Remove-Variable OFS
    $thread = [PowerShell]::Create().AddScript($Code)
    if($parameters){
        foreach($Key in $parameters.Keys){
            $thread.AddParameter($Key, $parameters.$Key)
        }
    }
    $thread.RunspacePool = $RunspacePool
    $Handle = $thread.BeginInvoke()
    $Job = "" | Select-Object Handle, Thread, object
    $Job.Handle = $Handle
    $Job.Thread = $thread
    $Job.Object = $secondaryScriptPath
    Write-Host "$(Get-Date): Starting new thread: $($Job.Object)"
    return $Job
}

$Jobs += startThread -communicatieObject $communicatieObject -fileName $communicatieObject.jobList[$jobIndex] -parameters ($communicatieObject.jobParameters.$jobIndex)
           